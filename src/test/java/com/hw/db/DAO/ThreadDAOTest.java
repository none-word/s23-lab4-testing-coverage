package com.hw.db.DAO;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ThreadDAOTest {
    @DisplayName("Test treeSort from ThreadDAO")
    @CsvSource(value = {
            "1-1-1-true-SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;",
            "--1--SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch;",
    }, delimiter = '-')
    @ParameterizedTest
    void treeSortTest(Integer id, Integer limit, Integer since, Boolean desc, String query) {
        JdbcTemplate jdbcTemplateMock = mock(JdbcTemplate.class);
        new ThreadDAO(jdbcTemplateMock);
        ThreadDAO.treeSort(id, limit, since, desc);
        verify(jdbcTemplateMock).query(
                eq(query),
                any(PostDAO.PostMapper.class), Optional.ofNullable(Mockito.anyVararg()));
    }
}