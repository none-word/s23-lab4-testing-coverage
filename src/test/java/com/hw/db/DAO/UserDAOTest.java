package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentMatchers;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Objects;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

class UserDAOTest {
    @DisplayName("Test change from UserDAO")
    @CsvSource(value = {
            "nickname-----true",
            "nickname---about-UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;-",
            "nickname--fullname--UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;-",
            "nickname--fullname-about-UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;-",
            "nickname-email---UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;-",
            "nickname-email--about-UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;-",
            "nickname-email-fullname--UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;-",
            "nickname-email-fullname-about-UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;-",
    }, delimiter = '-')
    @ParameterizedTest
    void changeTest(String nickname, String email, String fullName, String about, String query, Boolean isNever) {
        User user = new User(nickname, email, fullName, about);
        JdbcTemplate jdbcTemplateMock = mock(JdbcTemplate.class);
        new UserDAO(jdbcTemplateMock);
        UserDAO.Change(user);
        if (isNever != null && isNever) {
            verify(jdbcTemplateMock, never()).update(
                    eq(query),
                    any(Object[].class));
        } else {
            verify(jdbcTemplateMock).update(
                    eq(query),
                    Stream.of(email, fullName, about, nickname).filter(Objects::nonNull).map(ArgumentMatchers::eq).toArray()
            );
        }
    }
}