package com.hw.db.DAO;

import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class PostDAOTest {
    private static Post touch;

    @BeforeEach
    void setUp() {
        touch = new Post();
        touch.setAuthor("touchAuthor");
        touch.setMessage("touchMessage");
        touch.setCreated(new Timestamp(0));
    }

    @DisplayName("Test setPost from PostDAO")
    @CsvSource(value = {
            "0-touchAuthor-touchMessage-0--true",
            "0-author-touchMessage-0-UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;-",
            "0-touchAuthor-message-0-UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;-",
            "0-author-message-1-UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;-",
            "0-touchAuthor-touchMessage-1-UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;-",
            "0-touchAuthor-message-1-UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;-",
            "0-author-message-0-UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;-",
    }, delimiter = '-')
    @ParameterizedTest
    void setPostTest(Integer id, String author, String message, Long created, String query, Boolean isNever) {
        Post post = createPost(author, message, created);
        JdbcTemplate jdbcTemplateMock = mock(JdbcTemplate.class);
        when(jdbcTemplateMock.queryForObject(any(), any(PostDAO.PostMapper.class), eq(id))).thenReturn(touch);
        new PostDAO(jdbcTemplateMock);
        PostDAO.setPost(id, post);
        if (isNever != null && isNever) {
            verify(jdbcTemplateMock, never()).update(
                    eq(query),
                    any(Object[].class));
        } else {
            verify(jdbcTemplateMock).update(
                    eq(query),
                    Optional.ofNullable(Mockito.anyVararg())
            );
        }
    }

    private static Post createPost(String author, String message, Long created) {
        Post post = new Post();
        post.setAuthor(author);
        post.setMessage(message);
        post.setCreated(new Timestamp(created));
        return post;
    }
}