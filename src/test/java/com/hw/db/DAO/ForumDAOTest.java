package com.hw.db.DAO;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ForumDAOTest {
    @DisplayName("Test userList from ForumDAO")
    @CsvSource(value = {
            "test----SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;",
            "test--test--SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;",
            "test-1-test-true-SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;",
    }, delimiter = '-')
    @ParameterizedTest
    void userListTest(String slug, Integer limit, String since, Boolean desc, String query) {
        JdbcTemplate jdbcTemplateMock = mock(JdbcTemplate.class);
        new ForumDAO(jdbcTemplateMock);
        ForumDAO.UserList(slug, limit, since, desc);
        verify(jdbcTemplateMock).query(
                eq(query),
                any(Object[].class), any(UserDAO.UserMapper.class));
    }
}